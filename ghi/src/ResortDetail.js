import "./ResortDetails.css";
import { useState, useEffect } from "react";
import { NavLink, useParams } from "react-router-dom";

function ResortCard() {
  const [resortDetails, setResortDetails] = useState([]);
  const [summaryData, setSummaryData] = useState([]);
  const [weatherData, setWeatherData] = useState([]);

  const params = useParams();
  const resort_name = params.resort_name;

  useEffect(() => {
    const fetchWeatherData = async () => {
      const url = `http://localhost:8000/api/weather/${resort_name}`;
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setWeatherData(data.forecast5Day);
        setSummaryData(data.summary3Day);
      }
    };
    fetchWeatherData();

    const fetchResortData = async () => {
      const url = `http://localhost:8000/api/resorts/${resort_name}`;
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setResortDetails([data]);
      }
    };

    fetchResortData();

    const snowContainer = document.querySelector(".snow-container");

    for (let i = 0; i < 30; i++) {
      createSnowflake();
    }

    function createSnowflake() {
      const snowflake = document.createElement("div");
      snowflake.classList.add("snowflake", "snowflake-small", "snowflake-back");
      snowflake.style.left = `${Math.random() * window.innerWidth}px`;
      snowflake.style.animationDelay = `${Math.random() * 5}s`;
      snowContainer.appendChild(snowflake);
    }
  }, [resort_name]);

  return (
    <>
      <div className="snow-container">
        <div className="font-link">
          {resortDetails.map((resort) => (
            <div key={resort.id} className="resort-card">
              <h1
                style={{
                  color: "black",
                  textShadow: "2px 1px 6px white",
                  backgroundColor: "#0dd8b6ec",
                  textAlign: "center",
                  borderRadius: "50%",
                  marginTop: "1%",
                  padding: "20px",
                  width: "50%",
                  marginLeft: "25%",
                }}
              >
                welcome to {resort.resort_name}!
              </h1>

              <div className="row">
                <div className="col-md-9">
                  <img className="img-fluid" src={resort.trail_map} alt="" />
                </div>
                <div className="col-sm-2">
                  <div className="data-element rounded">
                    <p>State: {resort.state}</p>
                    <p>Summit: {resort.summit} ft</p>
                    <p>Base: {resort.base} ft</p>
                    <p>Vertical: {resort.vertical} ft</p>
                    <p>Latitude: {resort.lat}</p>
                    <p>Longitude: {resort.lon}</p>
                    <p>Lifts: {resort.lifts}</p>
                    <p>
                      Runs: {resort.runs}
                      <ul>
                        <li>Green %: {resort.green_percent}</li>
                        <li>Blue %: {resort.blue_percent}</li>
                        <li>Black %: {resort.black_percent}</li>
                      </ul>
                    </p>
                    <br></br>
                    <p>
                      Acres: {resort.acres}
                      <ul>
                        <li>Green Acres: {resort.green_acres}</li>
                        <li>Blue Acres: {resort.blue_acres}</li>
                        <li>Black Acres: {resort.black_acres}</li>
                      </ul>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>

      <div className="container">
        <div className="row">
          {resortDetails.map((resort) => (
            <div key={resort.id}>
              <h2
                style={{
                  textAlign: "center",
                  backgroundColor: "orange",
                  borderRadius: "30%",
                  width: "25%",
                  padding: "5px",
                  marginLeft: "37%",
                }}
              >
                <NavLink
                  to={resort.web_address}
                  className="card-link"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  visit official site
                </NavLink>
              </h2>
            </div>
          ))}
          <div className="col-md-6">
            <div className="weather-container">
              <div className="weather-row">
                <div className="weather-label">Weather</div>
                {weatherData.map((weather, id) => (
                  <div key={id} className="weather-day">
                    {weather.dayOfWeek}
                  </div>
                ))}
              </div>
              <div className="weather-row">
                <div className="weather-label">Max Temp</div>
                {weatherData.map((weather, id) => (
                  <div key={id} className="weather-data">
                    {weather.am.maxTemp}
                  </div>
                ))}
              </div>
              <div className="weather-row">
                <div className="weather-label">Snowfall</div>
                {weatherData.map((weather, id) => (
                  <div key={id} className="weather-data">
                    {weather.am.snow}
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <table className="table table-striped table-container">
              <thead>
                <tr>
                  <th>Snow Summary</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{summaryData}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}
export default ResortCard;
