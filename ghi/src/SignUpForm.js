import { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useSignUpMutation } from "./app/authApi";
import { updateField } from "./app/accountSlice";
import { useNavigate } from "react-router-dom";
import { preventDefault } from "./app/utils";

function SignUpForm() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { username, email, password } = useSelector((state) => state.account);
  const [signUp, { isSuccess }] = useSignUpMutation();
  const field = useCallback(
    (e) =>
      dispatch(updateField({ field: e.target.name, value: e.target.value })),
    [dispatch]
  );

  if (isSuccess) {
    navigate("/trips");
  }

  return (
    <div className="signup-bg-img">
      <div className="font-link">
        <div className="offset-7 col">
          <div className="">
            <form
              method="POST"
              onSubmit={preventDefault(signUp, () => ({
                username,
                email,
                password,
              }))}
              className="signup-form signup-background"
            >
              <h2 className="signup-form mt-3">SIGN UP</h2>
              <div className="">
                <label className="signup-form pt-3 pb-1" htmlFor="username">
                  username
                </label>
                <div className="form-group pb-2 signup-form">
                  <input
                    required
                    onChange={field}
                    value={username}
                    name="username"
                    className="form-control signup-form"
                    type="text"
                  />
                </div>
              </div>

              <div className="">
                <label className="signup-form pb-1">email</label>
                <div className="form-group pb-2 signup-form">
                  <input
                    required
                    onChange={field}
                    value={email}
                    name="email"
                    className="form-control signup-form"
                    type="email"
                  />
                </div>
              </div>
              <div className="">
                <label className="signup-form pb-1">password</label>
                <div>
                  <input
                    required
                    onChange={field}
                    value={password}
                    name="password"
                    className="form-control signup-form mb-5"
                    type="password"
                  />
                </div>
              </div>
              <div className="field is-grouped">
                <div className="form">
                  <button type="submit" className="btn signup-button mx-7">
                    sign up
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SignUpForm;
