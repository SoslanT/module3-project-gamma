import { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLogInMutation } from "./app/authApi";
import { updateField } from "./app/accountSlice";
import { useNavigate } from "react-router-dom";
import { preventDefault, eventTargetSelector as target } from "./app/utils";
import Notification from "./Notification";

function LogInForm() {
  const dispatch = useDispatch();
  const { username, password } = useSelector((state) => state.account);
  const [logIn, { error, isSuccess }] = useLogInMutation();
  const navigate = useNavigate();
  const field = useCallback(
    (e) =>
      dispatch(updateField({ field: e.target.name, value: e.target.value })),
    [dispatch]
  );

  if (isSuccess) {
    setTimeout(() => {
      navigate("/trips");
    }, 0);
  }

  return (
    <div className="login-bg-img">
      <div className="font-link">
        <div className="offset-6 col">
          <div className="">
            <form
              method="POST"
              onSubmit={preventDefault(logIn, target)}
              className="login-form login-background"
            >
              <h2 className="login-form">LOGIN</h2>
              <div className="">
                <label className="login-form pb-1" htmlFor="username">
                  username
                </label>
                <div className="login-form pb-2">
                  <input
                    required
                    onChange={field}
                    value={username}
                    name="username"
                    className="form-control login-input"
                    type="text"
                  />
                </div>
              </div>
              <div className="login-form">
                <label className="login-form pb-1">password</label>
                <div>
                  <input
                    required
                    onChange={field}
                    value={password}
                    name="password"
                    className="form-control login-input mb-5"
                    type="password"
                    placeholder=""
                  />
                </div>
              </div>
              <div className="field is-grouped">
                <button className="btn login-button mx-7">login</button>
                {error ? (
                  <div className="alert alert-danger m-4" role="alert">
                    <Notification type="danger">
                      {error.data.detail}
                    </Notification>
                  </div>
                ) : null}
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LogInForm;
