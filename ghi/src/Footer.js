import React from "react";
import { Link } from "react-router-dom";

import cait from "./photos/Cait.png";
import nathan from "./photos/Nathan.png";
import luis from "./photos/Luis.png";
import soslan from "./photos/Soslan.png";

const Footer = () => {
  return (
    <footer
      style={{
        backgroundColor: "rgb(228, 121, 15, 0.8)",
        color: "white",
        textAlign: "center",
        padding: "1rem",
        fontFamily: "'SpecialEliteRegular', sans-serif",
      }}
    >
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div style={{ marginRight: "1rem" }}>
          <img
            src={cait}
            alt="Cait"
            style={{ borderRadius: "10%", width: "50px", height: "50px" }}
          />
          <h4>Cait</h4>
          <p>
            Experienced snowboarder and
            <br />
            unintentional plant killer.
          </p>
          <Link className="footer-link" to="https://developer1-portfolio.com">
            Portfolio
          </Link>
        </div>
        <div style={{ marginRight: "1rem" }}>
          <img
            src={nathan}
            alt="Nathan"
            style={{ borderRadius: "10%", width: "50px", height: "50px" }}
          />
          <h4>Nathan</h4>
          <p>
            Texas forever <br />
            WhooooWhooo!
          </p>
          <Link className="footer-link" to="https://developer2-portfolio.com">
            Portfolio
          </Link>
        </div>
        <div style={{ marginRight: "1rem" }}>
          <img
            src={luis}
            alt="Luis"
            style={{ borderRadius: "10%", width: "50px", height: "50px" }}
          />
          <h4>Luis</h4>
          <p>
            Inexperienced snowboarder and
            <br /> plant revitalizer
          </p>
          <Link className="footer-link" to="https://developer3-portfolio.com">
            Portfolio
          </Link>
        </div>
        <div>
          <img
            src={soslan}
            alt="Soslan"
            style={{ borderRadius: "10%", width: "50px", height: "50px" }}
          />
          <h4>Soslan</h4>
          <p>
            Master of gibberish
            <br />
            and right hand of AI
          </p>
          <Link className="footer-link" to="https://developer4-portfolio.com">
            Portfolio
          </Link>
        </div>
      </div>
      <p style={{ fontFamily: "'SpecialEliteRegular', sans-serif" }}>
        Copyright © 2023
      </p>
    </footer>
  );
};

export default Footer;
