import "./App.css";
import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Nav from "./Nav";
import LogInForm from "./LogInForm";
import CreateTripForm from "./CreateTrip";
import TripList from "./TripList";
import SignUpForm from "./SignUpForm";
import ResortList from "./ResortList";
import ResortDetail from "./ResortDetail";
import { Provider } from "react-redux";
import { store } from "./app/store";
import MainPage from "./MainPage";
import Footer from "./Footer";


function App() {
  return (

    <BrowserRouter>
      <Provider store={store}>
        <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/login" element={<LogInForm />} />
          <Route path="/signup" element={<SignUpForm />} />

          <Route path="/trips" element={<TripList />} />
          <Route path="trips/new" element={<CreateTripForm />} />

          <Route path="/resorts" element={<ResortList />} />
          <Route path="resorts/:resort_name" element={<ResortDetail />} />

        </Routes>
        <Footer />

      </Provider>
    </BrowserRouter>
  );
}

export default App;
