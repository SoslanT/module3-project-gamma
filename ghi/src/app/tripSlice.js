import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  trips: [],
  isFetching: false,
  error: null,
}

export const tripsSlice = createSlice({
  name: 'trips',
  initialState,
  reducers: {
    addTrip: (state, action) => {
      state.trips.push(action.payload)
    },
    setTrips: (state, action) => {
      state.trips = action.payload
    },
    setIsFetching: (state, action) => {
      state.isFetching = action.payload
    },
    setError: (state, action) => {
      state.error = action.payload
    },
  },
})

export const { addTrip, setTrips, setIsFetching, setError } = tripsSlice.actions

export default tripsSlice.reducer
