const ADD_TRIP = 'ADD_TRIP'
const DELETE_TRIP = 'DELETE_TRIP'

const tripReducer = (state = [], action) => {
  switch (action.type) {
    case ADD_TRIP:
      return [...state, action.payload]
    case DELETE_TRIP:
      return state.filter((trip) => trip.id !== action.payload.id)
    default:
      return state
    case 'RESET_FORM':
      return {
        ...state,

      }
  }
}

export const addTrip = (trip) => ({ type: ADD_TRIP, payload: trip })
export const deleteTrip = (trip) => ({ type: DELETE_TRIP, payload: trip })
export const resetForm = () => ({
  type: 'RESET_FORM'
})
export default tripReducer
