import { useState } from "react";
import { useGetResortsQuery } from "./app/authApi";
import { Link } from "react-router-dom";

function ResortList() {
  const { data: resortsData } = useGetResortsQuery();
  const [stateFilter, setStateFilter] = useState("");

  const filteredResorts = resortsData?.resorts.filter((resort) =>
    resort.state.toLowerCase().includes(stateFilter.toLowerCase())
  );

  return (
    <>
      <div className="resort-list-background">
        <div className="font-link">
          <div className="d-flex p-2">
            <input
              className="form-control"
              type="text"
              placeholder="Search State"
              aria-label="Search"
              value={stateFilter}
              onChange={(e) => setStateFilter(e.target.value)}
            />
          </div>
          <div className="resort-main-title">resorts</div>
          <div className="row resort-row">
            <div className="col-sm-6">
              <iframe
                title="map"
                className="map"
                src="https://api.mapbox.com/styles/v1/caitcav/clet0mirb000q01nu6mtzcptk.html?title=false&access_token=pk.eyJ1IjoiY2FpdGNhdiIsImEiOiJjbGV0MGpjZG4wNnZxNDBsd2wzb3o5MTRwIn0.T5MeN-H6W8qL24tyF1FWNg&zoomwheel=false#4.64/39.75/-119.08"
              ></iframe>
            </div>
            <div className="col-md-5">
              <div className="trip-table-scroll">
                <table className="resort-list-table">
                  <thead className="resort-name-header">
                    <tr>
                      <th className="resort-name-header">RESORT NAME</th>
                      <th className="resort-name-header">STATE</th>
                    </tr>
                  </thead>
                  <tbody>
                    {filteredResorts?.map((resort) => (
                      <tr key={resort.resort_name}>
                        <td className="resort-name-link">
                          <Link
                            className="resort-name-link"
                            to={`/resorts/${resort.resort_name}`}
                          >
                            {resort.resort_name}
                          </Link>
                        </td>
                        <td className="state-name">{resort.state}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ResortList;
