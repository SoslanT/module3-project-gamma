from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from routers import accounts, auth, trips, resorts, jokes

app = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "http://localhost:3000",
        os.environ.get("CORS_HOST", "http://localhost:3000"),
        "http://localhost:8000",
        os.environ.get("CORS_HOST", "http://localhost:8000"),
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(accounts.router)
app.include_router(auth.authenticator.router)
app.include_router(trips.router)
app.include_router(resorts.router)
app.include_router(jokes.router)
