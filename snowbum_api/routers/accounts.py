from fastapi import (
    APIRouter,
    Request,
    Response,
    Depends,
    HTTPException,
    status,
)

from jwtdown_fastapi.authentication import Token
from .auth import authenticator

from pydantic import BaseModel

from queries.accounts import (
    AccountsIn,
    AccountsOut,
    AccountQueries,
    DuplicateAccountError,
)


class AccountToken(Token):
    account: AccountsOut


class AccountForm(BaseModel):
    username: str
    password: str


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.post("/api/accounts", response_model=AccountToken | HttpError)
async def create_account(
    info: AccountsIn,
    response: Response,
    request: Request,
    accounts: AccountQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = accounts.create(info, hashed_password)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = AccountForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, accounts)
    return AccountToken(account=account, **token.dict())


@router.put("/api/accounts/{id}", response_model=AccountsOut)
async def update_account(
    id: str,
    request: Request,
    account: AccountsIn,
    account_data: dict = Depends(authenticator.try_get_current_account_data),
    repo: AccountQueries = Depends(),
):
    if account_data and authenticator.cookie_name in request.cookies:
        return repo.update_account(account=account, id=id)
    else:
        return "Not working"


@router.delete("/api/accounts/{id}", response_model=bool)
async def delete_account(
    request: Request,
    id: str,
    repo: AccountQueries = Depends(),
    account_data: dict = Depends(authenticator.try_get_current_account_data),
):
    if account_data and authenticator.cookie_name in request.cookies:
        repo.delete_account(id=id)
        return True
    else:
        return "Not working"
