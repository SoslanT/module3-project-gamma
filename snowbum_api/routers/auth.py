import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.accounts import AccountQueries, AccountsOut


class MyAuthenticator(Authenticator):
    async def get_account_data(
        self,
        username: str,
        account: AccountQueries,
    ) -> AccountsOut:
        return account.get(username)

    def get_account_getter(
        self,
        account: AccountQueries = Depends(),
    ) -> AccountQueries:
        return account

    def get_hashed_password(self, account: AccountsOut):
        return account.password

    def get_account_data_for_cookie(self, account: AccountsOut):
        return account.username, AccountsOut(**account.dict())


authenticator = MyAuthenticator(os.environ["SIGNING_KEY"])
