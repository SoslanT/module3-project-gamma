from pydantic import BaseModel
from typing import List, Optional


class TripIn(BaseModel):
    name: str
    resort_name: str
    date: str
    description: Optional[str]


class TripOut(TripIn):
    id: str | int
    account_id: str | int


class TripList(BaseModel):
    trips: List[TripOut]


class TripHistory(BaseModel):
    trips: List[TripOut]


class AccountsIn(BaseModel):
    email: str
    username: str
    password: str


class AccountsOut(AccountsIn):
    id: str | int
    roles: List[str]


class AccountsList(BaseModel):
    accounts: List[AccountsOut]


class ResortDetail(BaseModel):
    id: Optional[str]
    resort_name: str
    state: str
    summit: Optional[str] = None
    base: Optional[str] = None
    vertical: Optional[str] = None
    lifts: Optional[str] = None
    runs: Optional[str] = None
    acres: Optional[str] = None
    green_percent: Optional[str] = None
    green_acres: Optional[str] = None
    blue_percent: Optional[str] = None
    blue_acres: Optional[str] = None
    black_percent: Optional[str] = None
    black_acres: Optional[str] = None
    lat: Optional[str] = None
    lon: Optional[str] = None
    web_address: Optional[str] = None
    trail_map: Optional[str] = None


class Resort(BaseModel):
    resort_name: str
    state: str


class ResortList(BaseModel):
    resorts: List[Resort]


class Joke(BaseModel):
    id: str
    joke: str
    status: int
