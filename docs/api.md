### Main paige (authorized)

- Endpoint path /main
- Endpoint method / GET/ token

- Headers:

  - Authorization: Bearer token

- Response: Main page, with navbar links and dropdowns

- Response shape(JSON):
  ```json
  {
   "text": string,
   "picture_url": string,
   "user_name": string,
   "search": query
  }
  ```

### Main page (unauthorized)

- Endpoint path /main

- Endpoint method / GET

- Response: Main page

\*Response shape(JSON):

```json
{
  "text": string,
  "picture_url": string,
}
```

### Log in

- Endpoint path/ token
- Endpoint method: POST

- Request shape(form):

  - username: string
  - password: string

- Response: Account information and token

- Response shape(JSON):
  ```json
  {
      "account": {
          "key": type,
      }
      "token": string,
  }
  ```

### Sign up

- Endpoint path: /token
- Endpoint method: POST

- Request shape (form):

  - first name: string
  - last name: string
  - username: string
  - password: string

- Response:

  - Created. Success.

- Response shape(JSON):
  ```json
  {
      "success": boolean,
      "message": string,
  }
  ```

### Log out

- Endpoint path/ token
- Endpoint method: DELETE

- Headers:

  - Authorization: Bearer token

- Response: Always true

- Response shape(JSON):
  ```json
  {
      true
  }
  ```

### Plan a trip

- Endpoint path: /trips/create
- Endpoint method: POST / token

- Headers:

  - Authorization: Bearer token

- Request shape (form):

  - name of the mountain: string
  - trip date: date type
  - details: text

- Response: Created. Success.

- Response shape(JSON):
  ```json
  {
      "success": boolean,
      "message": string,
  }
  ```

### Upcoming trips

- Endpoint path:/ trips/upcoming
- Endpoint method: GET/token

- Headers:

  - Authorization: Bearer token

- Response: list of upcoming trips (uncompleted)

- Response shape(JSON):
  ```json
  {
      "list":[
      {
          "id": number,
          "name": string,
          "date": date,
      }
      ]
  }
  ```

### Trips history

- Endpoint path:/ trips/history
- Endpoint method: GET /token

- Headers:

  - Authorization: Bearer token

- Response: List of past trips.

- Response shape(JSON):
  ```json
  {
      "list":[
          {
              "id": number,
              "name": string,
              "date": date,
          }
      ]
  }
  ```

### Find a mountain

- Endpoint path:/ find
- Endpoint method:/ GET/ token

- Headers:

  - Authorization: Bearer token

- Response: List of near mountains

- Response shape(JSON):
  ```json
  {
     "results": [
      {
          "id": int,
          "name": string,
          "location": string,
      }
     ]
  }
  ```
