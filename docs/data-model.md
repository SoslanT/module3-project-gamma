| Model | Fields | Type | Optional Columns |
| ----- | ------ | ---- | ---------------- |
| TripIn | name | str |  |
|       | mountain_name | str |  |
|       | date | str | yes |
|       | description | str | yes |
| TripOut | id | str/int |  |
|        | account_id | str/int |  |
|        | name | str |  |
|        | mountain_name | str |  |
|        | date | str | yes |
|        | description | str | yes |
| TripList | trips | List[TripOut] |  |
| TripHistory | trips | List[TripOut] |  |
| AccountsIn | email | str |  |
|            | username | str |  |
|            | password | str |  |
| AccountsOut | id | str/int |  |
|             | email | str |  |
|             | username | str |  |
|             | password | str |  |
|             | roles | List[str] |  |
| AccountsList | accounts | List[AccountsOut] |  |
| SessionOut | jti | str |  |
|            | account_id | str |  |
| ResortDetail | id | str | yes |
|               | resort_name | str |  |
|               | state | str |  |
|               | summit | str | yes |
|               | base | str | yes |
|               | vertical | str | yes |
|               | lifts | str | yes |
|               | runs | str | yes |
|               | acres | str | yes |
|               | green_percent | str | yes |
|               | green_acres | str | yes |
|               | blue_percent | str | yes |
|               | blue_acres | str | yes |
|               | black_percent | str | yes |
|               | black_acres | str | yes |
|               | lat | str | yes |
|               | lon | str | yes |
|               | web_address | str | yes |
|               | trail_map | str | yes |
| Resort | resort_name | str |  |
|        | state | str |  |
| ResortList | resorts | List[Resort] |  |
